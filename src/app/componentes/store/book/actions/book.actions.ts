
import { Action } from "@ngrx/store";
import { Book } from 'src/app/models/book.model';
export const BOOK_CARGAR = '[Book] Cargar Book';
export const BOOK_SUSSES = '[Book] Book exitoso';
export const BOOK_FAIL = '[Book] Book fail';


export class BookCargar implements Action {
    readonly type = BOOK_CARGAR;   
    constructor(public data: any){} 
} 

export class BookSusses implements Action {
    readonly type = BOOK_SUSSES;
    constructor(public book: Book){}
}

export class BookFail implements Action {
    readonly type = BOOK_FAIL;
    constructor(public payload: any){}
} 

export type bookAcciones = BookCargar |
                            BookSusses |
                            BookFail;