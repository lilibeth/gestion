import { Action } from "@ngrx/store";
export const BOOK_UPDATE_CARGAR = '[Book Update] Cargar Book Update';
export const BOOK_UPDATE_SUSSES = '[Book Update] Book Update exitoso';
export const BOOK_UPDATE_FAIL = '[Book Update ] Book Update fail';


export class BookUpdateCargar implements Action {
    readonly type = BOOK_UPDATE_CARGAR;   
    constructor(public data: any){} 
} 

export class BookUpdateSusses implements Action {
    readonly type = BOOK_UPDATE_SUSSES;
    constructor(public data: any){}
}

export class BookUpdateFail implements Action {
    readonly type = BOOK_UPDATE_FAIL;
    constructor(public payload: any){}
} 

export type bookUpdateAcciones = BookUpdateCargar |
                                BookUpdateSusses |
                                BookUpdateFail;