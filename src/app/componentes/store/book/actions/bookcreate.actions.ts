import { Action } from "@ngrx/store";
export const BOOK_CREATE_CARGAR = '[Book Create] Cargar Book create';
export const BOOK_CREATE_SUSSES = '[Book Create] Book create exitoso';
export const BOOK_CREATE_FAIL = '[Book Create ] Book create fail';


export class BookCreateCargar implements Action {
    readonly type = BOOK_CREATE_CARGAR;   
    constructor(public data: any){} 
} 

export class BookCreateSusses implements Action {
    readonly type = BOOK_CREATE_SUSSES;
    constructor(public data: any){}
}

export class BookCreateFail implements Action {
    readonly type = BOOK_CREATE_FAIL;
    constructor(public payload: any){}
} 

export type bookCreateAcciones = BookCreateCargar |
                                BookCreateSusses |
                                BookCreateFail;