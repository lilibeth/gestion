
import { Action } from "@ngrx/store";
import { Book } from 'src/app/models/book.model';
export const BOOKS_CARGAR = '[Books] Cargar Books';
export const BOOKS_SUSSES = '[Books] Books exitoso';
export const BOOKS_FAIL = '[Books] Books fail';


export class BooksCargar implements Action {
    readonly type = BOOKS_CARGAR;   
    constructor(public data: any){} 
} 

export class BooksSusses implements Action {
    readonly type = BOOKS_SUSSES;
    constructor(public books: Book[]){}
}

export class BooksFail implements Action {
    readonly type = BOOKS_FAIL;
    constructor(public payload: any){}
} 

export type booksAcciones = BooksCargar |
                            BooksSusses |
                            BooksFail;