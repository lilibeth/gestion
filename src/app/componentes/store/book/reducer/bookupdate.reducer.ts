import { bookUpdateAcciones, BOOK_UPDATE_FAIL, BOOK_UPDATE_SUSSES, BOOK_UPDATE_CARGAR } from '../actions/bookupdate.actions';

export interface BookUpdateState {    
    loaded: boolean;
    loading: boolean;
    error: any;
    bookUpdate: boolean;    
}

export const estadoInicialBookUpdate: BookUpdateState = {    
    loaded: false,
    loading: false,
    error: null,
    bookUpdate: false
}

export function bookUpdateReducer( state = estadoInicialBookUpdate, actions: bookUpdateAcciones): BookUpdateState{
    switch (actions.type) {
        case BOOK_UPDATE_CARGAR:            
            return {
                ...state,
                loading: true,
                error: null
            };
        case BOOK_UPDATE_SUSSES:   
            return {
                ...state,
                loading: false,
                loaded: true,
                bookUpdate: true
            }
        case BOOK_UPDATE_FAIL:
            return {
                 ...state,
                 loaded: false,
                 loading: false,
                 error: {
                     status: actions.payload.status,
                     message: actions.payload.message,
                     url: actions.payload.url
                 }
            }
    
        default:
            return state;
    }
}

