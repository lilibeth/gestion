import { booksAcciones, BOOKS_FAIL, BOOKS_CARGAR, BOOKS_SUSSES } from '../actions/books.actions';


export interface BooksState {    
    loaded: boolean;
    loading: boolean;
    error: any;
    books: any;    
}

export const estadoInicialBooks: BooksState = {    
    loaded: false,
    loading: false,
    error: null,
    books: []
}

export function booksReducer( state = estadoInicialBooks, actions: booksAcciones): BooksState{
    switch (actions.type) {
        case BOOKS_CARGAR:            
            return {
                ...state,
                loading: true,
                error: null
            };
        case BOOKS_SUSSES:    
                  
            return {
                ...state,
                loading: false,
                loaded: true,
                books: [
                    ...actions.books
                ]
            }
        case BOOKS_FAIL:
            return {
                 ...state,
                 loaded: false,
                 loading: false,
                 error: {
                     status: actions.payload.status,
                     message: actions.payload.message,
                     url: actions.payload.url
                 }
            }
    
        default:
            return state;
    }
}

