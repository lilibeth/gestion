import { BOOK_CREATE_CARGAR, BOOK_CREATE_SUSSES, BOOK_CREATE_FAIL, bookCreateAcciones } from '../actions/bookcreate.actions';




export interface BookCreateState {    
    loaded: boolean;
    loading: boolean;
    error: any;
    bookCreate: boolean;    
}

export const estadoInicialBookCreate: BookCreateState = {    
    loaded: false,
    loading: false,
    error: null,
    bookCreate: false
}

export function bookCreateReducer( state = estadoInicialBookCreate, actions: bookCreateAcciones): BookCreateState{
    switch (actions.type) {
        case BOOK_CREATE_CARGAR:            
            return {
                ...state,
                loading: true,
                error: null
            };
        case BOOK_CREATE_SUSSES:   
            console.log('klaskskl')
            return {
                ...state,
                loading: false,
                loaded: true,
                bookCreate: true
            }
        case BOOK_CREATE_FAIL:
            return {
                 ...state,
                 loaded: false,
                 loading: false,
                 error: {
                     status: actions.payload.status,
                     message: actions.payload.message,
                     url: actions.payload.url
                 }
            }
    
        default:
            return state;
    }
}

