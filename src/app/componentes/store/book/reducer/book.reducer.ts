import { bookAcciones, BOOK_CARGAR, BOOK_SUSSES, BOOK_FAIL } from '../actions/book.actions';



export interface BookState {    
    loaded: boolean;
    loading: boolean;
    error: any;
    book: any;    
}

export const estadoInicialBook: BookState = {    
    loaded: false,
    loading: false,
    error: null,
    book: {}
}

export function bookReducer( state = estadoInicialBook, actions: bookAcciones): BookState{
    switch (actions.type) {
        case BOOK_CARGAR:            
            return {
                ...state,
                loading: true,
                error: null
            };
        case BOOK_SUSSES:    
            return {
                ...state,
                loading: false,
                loaded: true,
                book: {
                    ...actions.book
                }
            }
        case BOOK_FAIL:
            return {
                 ...state,
                 loaded: false,
                 loading: false,
                 error: {
                     status: actions.payload.status,
                     message: actions.payload.message,
                     url: actions.payload.url
                 }
            }
    
        default:
            return state;
    }
}

