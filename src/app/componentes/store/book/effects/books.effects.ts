
import { Injectable } from "@angular/core";
import { Actions, Effect, ofType } from "@ngrx/effects";
import { of } from 'rxjs';
import { switchMap, map, catchError } from "rxjs/operators";
import { BookService } from 'src/app/services/book.service';
import { BOOKS_CARGAR, BooksSusses, BooksFail } from '../actions/books.actions';
import { BOOK_CARGAR, BookSusses, BookFail } from '../actions/book.actions';
import { BOOK_CREATE_CARGAR, BookCreateFail, BookCreateSusses } from '../actions/bookcreate.actions';
import { BOOK_UPDATE_CARGAR, BookUpdateSusses, BookUpdateFail } from '../actions/bookupdate.actions';

@Injectable()
export class BooksEffects {

    constructor(private actions$ : Actions, public booksService: BookService){}

    @Effect()
    cargarBooks$ = this.actions$.pipe(
                         ofType( BOOKS_CARGAR ),        
                            switchMap( action => {         
                                                 
                               return this.booksService.getBooks(action['data'])
                                              .pipe(                                                  
                                                  map( login =>  new BooksSusses( login )
                                                  ),
                                                  catchError(error => {
                                                    let _error = error;  
                                                    if(error['error'] && error['error']['errors']){                          
                                                        _error = error['error']['errors'][0];
                                                    } 
                                                    return of( new BooksFail( _error ));
                                                   })
                                              );
                            })
                        );

    @Effect()
    cargarBook$ = this.actions$.pipe(
                            ofType( BOOK_CARGAR ),        
                            switchMap( action => {        
                                                    
                                return this.booksService.getBook(action['data'])
                                                .pipe(                                                  
                                                    map( login =>  new BookSusses( login )
                                                    ),
                                                    catchError(error => {
                                                    let _error = error;  
                                                    if(error['error'] && error['error']['errors']){                          
                                                        _error = error['error']['errors'][0];
                                                    } 
                                                    return of( new BookFail( _error ));
                                                    })
                                                );
                            })
                        );
    
    @Effect()
    cargarBookCreate$ = this.actions$.pipe(
                            ofType( BOOK_CREATE_CARGAR ),        
                            switchMap( action => {                  
                                return this.booksService.createBook(action['data'])
                                                .pipe(                                                  
                                                    map( bookCreate =>  new BookCreateSusses( bookCreate )
                                                    ),
                                                    catchError(error => {
                                                    let _error = error;  
                                                    if(error['error'] && error['error']['errors']){                          
                                                        _error = error['error']['errors'][0];
                                                    } 
                                                    return of( new BookCreateFail( _error ));
                                                    })
                                                );
                            })
                        );
    
    @Effect()
    cargarBookUpdate$ = this.actions$.pipe(
                                ofType( BOOK_UPDATE_CARGAR ),        
                                switchMap( action => {                  
                                    return this.booksService.updateBook(action['data'])
                                                    .pipe(                                                  
                                                        map( bookCreate =>  new BookUpdateSusses( bookCreate )
                                                        ),
                                                        catchError(error => {
                                                        let _error = error;  
                                                        if(error['error'] && error['error']['errors']){                          
                                                            _error = error['error']['errors'][0];
                                                        } 
                                                        return of( new BookUpdateFail( _error ));
                                                        })
                                                    );
                                })
                            );
    
    


}
