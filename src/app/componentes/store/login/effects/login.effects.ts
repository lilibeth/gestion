import { Injectable } from "@angular/core";
import { Actions, Effect, ofType } from "@ngrx/effects";
import { of } from 'rxjs';
import { switchMap, map, catchError } from "rxjs/operators";
import { LoginService } from 'src/app/services/login.service';
import { LOGIN_CARGAR, LoginSusses, LoginFail } from '../actions/login.actions';

@Injectable()
export class LoginEffects {

    constructor(private actions$ : Actions, public loginService: LoginService){}

    @Effect()
    cargarLogin$ = this.actions$.pipe(
                         ofType( LOGIN_CARGAR ),        
                            switchMap( action => {         
                                                 
                               return this.loginService.login(action['data'])
                                              .pipe(                                                  
                                                  map( login =>  new LoginSusses( login )
                                                  ),
                                                  catchError(error => {
                                                    let _error = error;  
                                                    if(error['error'] && error['error']['errors']){                          
                                                        _error = error['error']['errors'][0];
                                                    } 
                                                    return of( new LoginFail( _error ));
                                                   })
                                              );
                            })
                        );


}
