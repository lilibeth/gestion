import { loginAcciones, LOGIN_CARGAR, lOGIN_SUSSES, LOGIN_FAIL, LOGOUT } from '../actions/login.actions';


export interface LoginState {    
    loaded: boolean;
    loading: boolean;
    error: any;
    logeado: boolean;
    user: any
}

export const estadoInicialLogin: LoginState = {    
    loaded: false,
    loading: false,
    error: null,
    logeado:false,
    user: null
}

export function loginReducer( state = estadoInicialLogin, actions: loginAcciones): LoginState{
    switch (actions.type) {
        case LOGIN_CARGAR:            
            return {
                ...state,
                loading: true,
                error: null
            };
        case lOGIN_SUSSES:      
            return {
                ...state,
                loading: false,
                loaded: true,
                logeado: true,
                user: {
                    ...actions.data
                }
            }
        case LOGIN_FAIL:
            return {
                 ...state,
                 loaded: false,
                 loading: false,
                 error: {
                     status: actions.payload.status,
                     message: actions.payload.message,
                     url: actions.payload.url
                 }
            }
        case LOGOUT:
                return {
                    ...state,
                    loading: false,
                    loaded: true,
                    logeado: false
                }
    
        default:
            return state;
    }
}

