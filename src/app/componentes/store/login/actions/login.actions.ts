import { Action } from "@ngrx/store";
export const LOGIN_CARGAR = '[Login] Cargar login';
export const lOGIN_SUSSES = '[Longin] Login exitoso';
export const LOGIN_FAIL = '[Login] Login fail';
export const LOGOUT = '[Login] Logout'

export class LoginCargar implements Action {
    readonly type = LOGIN_CARGAR;   
    constructor(public data: any){} 
} 

export class LoginSusses implements Action {
    readonly type = lOGIN_SUSSES;
    constructor(public data){}
}

export class LoginFail implements Action {
    readonly type = LOGIN_FAIL;
    constructor(public payload: any){}
} 

export class Logout implements Action {
    readonly type = LOGOUT;
    constructor(public data){}
}

export type loginAcciones = LoginCargar |
                            LoginSusses |
                            LoginFail |
                            Logout;