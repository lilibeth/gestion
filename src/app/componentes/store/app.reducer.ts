import { ActionReducerMap } from '@ngrx/store';
import { LoginState, loginReducer } from './login/reducer/login.reducer';
import { RegisterState, registerReducer } from './register/reducer/register.reducer';
import { BooksState, booksReducer } from './book/reducer/books.reducer';
import { BookState, bookReducer } from './book/reducer/book.reducer';
import { BookCreateState, bookCreateReducer } from './book/reducer/bookcreate.reducer';
import { BookUpdateState, bookUpdateReducer } from './book/reducer/bookupdate.reducer';

export interface AppState{
    login: LoginState;
    register: RegisterState;
    books: BooksState,
    book: BookState,
    bookCreate: BookCreateState,
    bookUpdate: BookUpdateState
   }
 
 export const appReducers: ActionReducerMap<AppState> = {
     login: loginReducer    ,
     register: registerReducer,
     books: booksReducer,
     book: bookReducer,
     bookCreate: bookCreateReducer,
     bookUpdate: bookUpdateReducer
 }