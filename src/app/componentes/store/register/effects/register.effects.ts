import { Injectable } from "@angular/core";
import { Actions, Effect, ofType } from "@ngrx/effects";
import { of } from 'rxjs';
import { switchMap, map, catchError } from "rxjs/operators";
import { RegisterService } from 'src/app/services/register.service';
import { REGISTER_CARGAR, RegisterFail, RegisterSusses } from '../actions/register.actions';


@Injectable()
export class RegisterEffects {

    constructor(private actions$ : Actions, public regiterService: RegisterService){}

    @Effect()
    cargarRegister$ = this.actions$.pipe(
                         ofType( REGISTER_CARGAR ),        
                            switchMap( action => {         
                                                 
                               return this.regiterService.register(action['data'])
                                              .pipe(                                                  
                                                  map( register =>  new RegisterSusses( register )
                                                  ),
                                                  catchError(error => {                                                      
                                                    let _error = error;  
                                                    if(error['error'] && error['error']['errors']){                          
                                                        _error = error['error']['errors'][0];
                                                    } 
                                                    return of( new RegisterFail( _error ));
                                                   })
                                              );
                            })
                        );


}
