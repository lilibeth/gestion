import { Action } from "@ngrx/store";
export const REGISTER_CARGAR = '[Register] Cargar register';
export const REGISTER_SUSSES = '[Register] Register exitoso';
export const REGISTER_FAIL = '[Register] Register fail';


export class RegisterCargar implements Action {
    readonly type = REGISTER_CARGAR;   
    constructor(public data: any){} 
} 

export class RegisterSusses implements Action {
    readonly type = REGISTER_SUSSES;
    constructor(public data){}
}

export class RegisterFail implements Action {
    readonly type = REGISTER_FAIL;
    constructor(public payload: any){}
} 


export type registerAcciones = RegisterCargar |
                               RegisterSusses |
                               RegisterFail ;