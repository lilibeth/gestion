import { registerAcciones, REGISTER_FAIL, REGISTER_SUSSES, REGISTER_CARGAR } from '../actions/register.actions';

export interface RegisterState {    
    loaded: boolean;
    loading: boolean;
    error: any;
    registrado: boolean;
}

export const estadoInicialRegister: RegisterState = {    
    loaded: false,
    loading: false,
    error: null,
    registrado:false
}

export function registerReducer( state = estadoInicialRegister, actions: registerAcciones): RegisterState{
    switch (actions.type) {
        case REGISTER_CARGAR:            
            return {
                ...state,
                loading: true,
                error: null
            };
        case REGISTER_SUSSES:
            return {
                ...state,
                loading: false,
                loaded: true,
                registrado: true
            }
        case REGISTER_FAIL:
            return {
                 ...state,
                 loaded: false,
                 loading: false,
                 error: {
                     status: actions.payload.status,
                     message: actions.payload.message,
                     url: actions.payload.url
                 }
            }
    
        default:
            return state;
    }
}

