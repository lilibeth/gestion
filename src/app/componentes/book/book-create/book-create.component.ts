import { Component, OnInit } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { Store } from '@ngrx/store';
import { AppState } from '../../store/app.reducer';
import { Book } from 'src/app/models/book.model'
import { BookCreateCargar } from '../../store/book/actions/bookcreate.actions';

@Component({
  selector: 'app-book-create',
  templateUrl: './book-create.component.html',
  styleUrls: ['./book-create.component.css']
})
export class BookCreateComponent implements OnInit {

  createForm: FormGroup;
  mensajeError: Boolean;
  mensajeExito: Boolean;
  data = {
    book: null,
    user: null
  }    

  constructor(private formBuilder: FormBuilder, private store: Store<AppState>) { 
    this.createFormBook()
  }

  ngOnInit() { 
    this.store.select('login').subscribe(login => {     
      this.data.user = login.user
    }).unsubscribe();  
  }

  createFormBook(){
    this.createForm = this.formBuilder.group(
      {
        title: ['', [Validators.required, Validators.minLength(8)]],
        description: ['', [Validators.required, Validators.minLength(8)]],
        pageCount: ['', [Validators.required, Validators.minLength(8)]],
        excerpt: ['', [Validators.required, Validators.minLength(8)]],
        publishDate: ['', [Validators.required, Validators.minLength(8)]]
      }
    )
  }

  register(): void {        
    let book = new Book();
    book.Title = this.createForm.controls['title'].value;
    book.Description = this.createForm.controls['description'].value;  
    book.PageCount = this.createForm.controls['pageCount'].value;  
    book.Excerpt = this.createForm.controls['excerpt'].value;  
    book.PublishDate =   this.createForm.controls['publishDate'].value;     
    this.data.book = book;
    if(this.data.user.access_token && this.data.book)  {      
      this.store.dispatch(new BookCreateCargar(this.data));
      this.store.select('bookCreate').subscribe(bookCreate => {            
        if(bookCreate.bookCreate){       
          this.mensajeExito  = true;
        }
        if(bookCreate.error){
          this.mensajeError = true;
        }
      })
    }
  }

}
