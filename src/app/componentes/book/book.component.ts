import { Component, OnInit } from '@angular/core';
import { Book } from 'src/app/models/book.model';
import { BookCargar } from '../store/book/actions/book.actions';
import { Store } from '@ngrx/store';
import { AppState } from '../store/app.reducer';
import { ActivatedRoute } from '@angular/router';

@Component({
  selector: 'app-book',
  templateUrl: './book.component.html',
  styleUrls: ['./book.component.css']
})
export class BookComponent implements OnInit {

  book: Book;

  constructor(private store: Store<AppState>, private rutaActiva: ActivatedRoute) { }

  ngOnInit() {
    let data = {
      id:  this.rutaActiva.snapshot.params.id,
      user: null
    }

    this.store.select('login').subscribe(login => {     
        data.user = login.user
    });  
    if(data.user && data.id)  {      
      this.store.dispatch(new BookCargar(data));    
      this.store.select('book').subscribe(book => {         
        this.book = book.book;
      });
    }  
  }

}
