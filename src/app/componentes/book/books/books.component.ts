import { Component, OnInit, OnDestroy } from '@angular/core';
import { Book } from 'src/app/models/book.model';
import { Store } from '@ngrx/store';
import { AppState } from '../../store/app.reducer';
import { BooksCargar } from '../../store/book/actions/books.actions';

@Component({
  selector: 'app-books',
  templateUrl: './books.component.html',
  styleUrls: ['./books.component.css']
})
export class BooksComponent implements OnInit {
  books: Book[];
  constructor(private store: Store<AppState>) { }

  ngOnInit() {
    let data : {
      id: "1"
    }
    this.store.dispatch(new BooksCargar(data));    
    this.store.select('books').subscribe(books => {     
       this.books = books.books;
    });    
  }

}
