import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { BookComponent } from './book.component';
import { BooksComponent } from './books/books.component';
import { RouterModule, Routes } from '@angular/router';
import { CanActivateViaAuthGuard } from 'src/app/guards/can-activate.guard';
import { BookService } from 'src/app/services/book.service';
import { EffectsModule } from '@ngrx/effects';
import { BooksEffects } from '../store/book/effects/books.effects';
import { MatTooltipModule, MatTableModule, MatPaginatorModule, MatSelectModule } from '@angular/material';
import { BookCreateComponent } from './book-create/book-create.component';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { RxReactiveFormsModule } from '@rxweb/reactive-form-validators';


const routes: Routes = [
  
  { path: 'create', component: BookCreateComponent, canActivate: [CanActivateViaAuthGuard]},
  { path: '', component: BooksComponent , canActivate: [CanActivateViaAuthGuard]} ,
  { path: ':id', component: BookComponent , canActivate: [CanActivateViaAuthGuard]}
];

@NgModule({
  declarations: [BookComponent , BooksComponent, BookCreateComponent],
  imports: [
    CommonModule,
    RouterModule.forChild(routes),    
    FormsModule,
    ReactiveFormsModule,
    RxReactiveFormsModule,
    EffectsModule.forRoot([BooksEffects]),    
    MatTooltipModule
  ],
  providers: [
    BookService
  ],
  exports: [
    BookComponent,
    BooksComponent
  ]
})
export class BookModule { }
