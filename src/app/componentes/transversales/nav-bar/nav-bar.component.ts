import { Component, OnInit } from '@angular/core';
import { Store } from '@ngrx/store';
import { AppState } from '../../store/app.reducer';

@Component({
  selector: 'app-nav-bar',
  templateUrl: './nav-bar.component.html',
  styleUrls: ['./nav-bar.component.css']
})
export class NavBarComponent implements OnInit {
   logeado: Boolean;
  constructor(private store: Store<AppState>) { }

  ngOnInit() {
    this.store.select('login').subscribe(login => {      
      if(login.logeado){       
        this.logeado = login.logeado       
      }
    });
  }
}
