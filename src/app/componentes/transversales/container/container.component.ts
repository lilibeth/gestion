import { Component, OnInit, OnChanges, Input } from '@angular/core';
import { Router } from '@angular/router';
import { Store } from '@ngrx/store';
import { AppState } from '../../store/app.reducer';
import { Logout } from '../../store/login/actions/login.actions';
import { LoginService } from 'src/app/services/login.service';

@Component({
  selector: 'app-container',
  templateUrl: './container.component.html',
  styleUrls: ['./container.component.css']
})
export class ContainerComponent implements OnInit {
  @Input() logeado: Boolean;
  constructor(private router: Router, private store: Store<AppState>, private loginServices: LoginService) { }

  ngOnInit() {    
    this.store.select('login').subscribe(login => {      
      if(login.logeado){       
        this.logeado = login.logeado       
      }
    });  
  }
 
  cerrarSession(){
    localStorage.setItem('IsLoggedIn', 'false');    
    let data = [];
    this.store.dispatch(new Logout(data));   
    this.store.select('login').subscribe(login => {      
      if(login.user){             
        this.loginServices.logouth(login.user);
        this.logeado = login.logeado ;
      }
    });  
  }

}
