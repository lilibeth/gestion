import { Component, OnInit } from '@angular/core';
import { Store } from '@ngrx/store';
import { AppState } from '../store/app.reducer';
import { User } from 'src/app/models/user.model';
import { LoginService } from 'src/app/services/login.service';

@Component({
  selector: 'app-user',
  templateUrl: './user.component.html',
  styleUrls: ['./user.component.css']
})
export class UserComponent implements OnInit {
  user: any;

  constructor(private store: Store<AppState>, private loginServices: LoginService) { }

  ngOnInit() {
    this.store.select('login').subscribe(login => {     
      if(login.user){    
          
        this.loginServices.getInfoUser(login.user).subscribe(user => {          
          this.user = { 
            ...user
          };
        })
      }
    }); 
  }

}
