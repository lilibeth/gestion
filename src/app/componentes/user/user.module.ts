import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { UserComponent } from './user.component';
import { UserbooksComponent } from './userbooks/userbooks.component';
import { UserbookseditComponent } from './userbooksedit/userbooksedit.component';
import { Routes, RouterModule } from '@angular/router';
import { CanActivateViaAuthGuard } from 'src/app/guards/can-activate.guard';
import { EffectsModule } from '@ngrx/effects';

import { MatTooltipModule } from '@angular/material';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { RxReactiveFormsModule } from '@rxweb/reactive-form-validators';
import { BooksEffects } from '../store/book/effects/books.effects';
import { LoginService } from 'src/app/services/login.service';

const routes: Routes = [
  
  { path: 'books', component: UserbooksComponent, canActivate: [CanActivateViaAuthGuard] },
  { path: 'books/:id/edit', component: UserbookseditComponent, canActivate: [CanActivateViaAuthGuard] },
  { path: '', component: UserComponent , canActivate: [CanActivateViaAuthGuard]}
];


@NgModule({
  declarations: [UserComponent, UserbooksComponent, UserbookseditComponent],
  imports: [
    CommonModule,
    RouterModule.forChild(routes),
    FormsModule,
    ReactiveFormsModule,
    RxReactiveFormsModule,
    EffectsModule.forRoot([BooksEffects]),
    MatTooltipModule
  ],
  providers: [
    LoginService
  ]
})
export class UserModule { }
