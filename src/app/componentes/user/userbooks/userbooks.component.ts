import { Component, OnInit } from '@angular/core';
import { BooksCargar } from '../../store/book/actions/books.actions';
import { Book } from 'src/app/models/book.model';
import { AppState } from '../../store/app.reducer';
import { Store } from '@ngrx/store';

@Component({
  selector: 'app-userbooks',
  templateUrl: './userbooks.component.html',
  styleUrls: ['./userbooks.component.css']
})
export class UserbooksComponent implements OnInit {
  books: Book[];
  constructor(private store: Store<AppState>) { }

  ngOnInit() {
    let data : {
      id: "1"
    }
    this.store.dispatch(new BooksCargar(data));    
    this.store.select('books').subscribe(books => {     
       this.books = books.books;
    });
    
  }

}
