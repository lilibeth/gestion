import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { UserbookseditComponent } from './userbooksedit.component';

describe('UserbookseditComponent', () => {
  let component: UserbookseditComponent;
  let fixture: ComponentFixture<UserbookseditComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ UserbookseditComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(UserbookseditComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
