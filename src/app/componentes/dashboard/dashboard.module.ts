import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { DashboardComponent } from './dashboard.component';
import { Routes, RouterModule } from '@angular/router';
import { CanActivateViaAuthGuard } from 'src/app/guards/can-activate.guard';


const routes: Routes = [
  { path: '', component: DashboardComponent , canActivate: [CanActivateViaAuthGuard]},
];

@NgModule({
  declarations: [DashboardComponent],
  imports: [
    CommonModule,
    RouterModule.forChild(routes)
  ],
  exports: [
    DashboardComponent
  ]
})
export class DashboardModule { }
