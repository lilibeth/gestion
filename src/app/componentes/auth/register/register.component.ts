import { Component, OnInit } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { RxwebValidators } from '@rxweb/reactive-form-validators';
import { User } from 'src/app/models/user.model';
import { RegisterCargar } from '../../store/register/actions/register.actions';
import { Store } from '@ngrx/store';
import { AppState } from '../../store/app.reducer';

@Component({
  selector: 'app-register',
  templateUrl: './register.component.html',
  styleUrls: ['./register.component.css']
})
export class RegisterComponent implements OnInit {
  createUserForm: FormGroup;
  mensajeError: Boolean;
  mensajeExitoso: Boolean;

  constructor(private formBuilder: FormBuilder,  private router: Router, private store: Store<AppState>) {
    this.createFormLogin();
   }

  ngOnInit() {
  }
   
  createFormLogin(){
    this.createUserForm = this.formBuilder.group(
      {
        names: ['', [Validators.required]],
        email: ['', [Validators.required, Validators.email]],
        confirmEmail: ['', [Validators.required, Validators.email, RxwebValidators.compare({fieldName:'email' })]],
        password: ['', [Validators.required, Validators.minLength(8)]],
        confirmPassword: ['', [Validators.required, Validators.minLength(8), RxwebValidators.compare({fieldName:'password' })]]
      }
    )
  }

  register(): void{        
    if(this.compareEmail()){      
      let user = new User();
      user.email = this.createUserForm.controls['email'].value;
      user.password = this.createUserForm.controls['password'].value;   
      user.name =  this.createUserForm.controls['names'].value;
      user.password_confirmation = this.createUserForm.controls['confirmPassword'].value;   
      this.store.dispatch(new RegisterCargar(user));    
      this.store.select('register').subscribe(register => {      
        if(register.registrado){        
          localStorage.setItem("names", user.name) ;
          localStorage.setItem("email", user.email) ;
          this.mensajeExitoso = true;          
        }
        if(register.error){
          this.mensajeError = true;
        }
      });
    }    
    
  }

  
  compareEmail(): boolean {
    if(this.createUserForm.controls['email'].value === this.createUserForm.controls['confirmEmail'].value){
      return true;
    }
    return false;
  }
}
