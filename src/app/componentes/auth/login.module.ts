import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { Routes, RouterModule } from '@angular/router';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { LoginComponent } from './login/login.component';
import { RegisterComponent } from './register/register.component';
import { RxReactiveFormsModule } from '@rxweb/reactive-form-validators';
import { IndexComponent } from './index/index.component';
import { EffectsModule } from '@ngrx/effects';
import { LoginEffects } from '../store/login/effects/login.effects';
import { LoginService } from 'src/app/services/login.service';
import { RegisterEffects } from '../store/register/effects/register.effects';

const routes: Routes = [
  { path: '', component: IndexComponent},
  { path: 'login', component: LoginComponent },
  { path: 'register', component: RegisterComponent },
];

@NgModule({
  declarations: [
    LoginComponent,
    RegisterComponent,
    IndexComponent
  ],
  imports: [
    CommonModule,
    RouterModule.forChild(routes),
    FormsModule,
    ReactiveFormsModule,
    RxReactiveFormsModule,
    EffectsModule.forRoot([LoginEffects, RegisterEffects])
  ],
  providers: [
    LoginService
  ],
  exports:[
    LoginComponent,
    RegisterComponent,
    IndexComponent
  ]
})
export class LoginModule { }
