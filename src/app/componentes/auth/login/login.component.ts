import { Component, OnInit } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { Store } from '@ngrx/store';
import { AppState } from '../../store/app.reducer';
import { LoginCargar } from '../../store/login/actions/login.actions';
import { User } from 'src/app/models/user.model';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {

  loginForm: FormGroup;
  regexPassword = /^[a-zA-Z0-9]+$/;
  mensajeError: Boolean;

  constructor(private formBuilder: FormBuilder, private router: Router, private store: Store<AppState>) { 
    this.createFormLogin()
  }

  ngOnInit() { 
  }

  createFormLogin(){
    this.loginForm = this.formBuilder.group(
      {
        username: ['', [Validators.required, Validators.email]],
        password: ['', [Validators.required, Validators.minLength(8)]],
        names: ['', [Validators.required, Validators.minLength(8)]]
      }
    )
  }

  login(): void {    
    localStorage.setItem("username", this.loginForm.controls['username'].value );
    let user = new User();
    user.email = this.loginForm.controls['username'].value;
    user.password = this.loginForm.controls['password'].value;   
    user.name =  this.loginForm.controls['names'].value;   
    this.store.dispatch(new LoginCargar(user));    
    this.store.select('login').subscribe(login => {  
        
      if(login.user){        
        localStorage.setItem("IsLoggedIn", 'true') ;
        this.router.navigateByUrl("/dashboard");
        
      }
      if(login.error){
         this.mensajeError = true;
      }
    });
  }


}
