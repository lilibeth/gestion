import { Injectable } from "@angular/core";
import { CanActivate, Router } from "@angular/router";
import { Store, select } from '@ngrx/store';
import { AppState } from '../componentes/store/app.reducer';
import { Observable, of } from 'rxjs';
import { map } from 'rxjs/operators';



@Injectable()
export class CanActivateViaAuthGuard implements CanActivate{
    constructor(private router: Router, private store: Store<AppState>){}   

    canActivate(): Observable<boolean> {
        return this.store.pipe(
          select('login'),
          map(login => {              
            if (!login.logeado) {                
              this.router.navigateByUrl("/login");
              return false;
            }           
            return true;
          })
        );
      }
}