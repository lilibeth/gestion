import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { NavBarComponent } from './componentes/transversales/nav-bar/nav-bar.component';
import { CanActivateViaAuthGuard } from './guards/can-activate.guard';
import { ContainerComponent } from './componentes/transversales/container/container.component';
import { environment } from 'src/environments/environment';
import { StoreModule } from '@ngrx/store';
import { StoreDevtoolsModule } from '@ngrx/store-devtools';
import { appReducers } from './componentes/store/app.reducer';
import { HttpClientModule } from '@angular/common/http';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { LoginService } from './services/login.service';

@NgModule({
  declarations: [
    AppComponent,
    NavBarComponent,
    ContainerComponent        
  ],
  imports: [
    BrowserModule,    
    BrowserAnimationsModule,
    AppRoutingModule,
    HttpClientModule,
    StoreModule.forRoot(appReducers),   
    StoreDevtoolsModule.instrument({
      maxAge: 25,
      logOnly: environment.production
    })  
  ],
  providers: [
    CanActivateViaAuthGuard,
    LoginService
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
