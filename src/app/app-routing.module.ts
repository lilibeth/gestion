import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';


const routes: Routes = [

  {
    path: '',
    loadChildren: './componentes/auth/login.module#LoginModule'
  },
  {
     path: 'dashboard',     
     loadChildren: './componentes/dashboard/dashboard.module#DashboardModule'
  },
  {
    path: 'book',
    loadChildren: './componentes/book/book.module#BookModule'
  },
  {
    path: 'user',
    loadChildren: './componentes/user/user.module#UserModule'
  }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]

})
export class AppRoutingModule { }
