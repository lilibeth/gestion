import { Injectable } from '@angular/core';
import { environment } from 'src/environments/environment';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { map } from 'rxjs/operators';

@Injectable({
  providedIn: 'root'
})
export class LoginService {

  private apiUrl: string = environment.urlApi; 
  httpHeaders: HttpHeaders;


  constructor( private http: HttpClient) { 
    this.httpHeaders = new HttpHeaders({
      'Content-type': 'application/json',       
      'X-Requested-With': 'XMLHttpRequest'   
    }); 
  }


  login(request){    
    return this.http.post("http://127.0.0.1:8000/api/auth/login", request, {headers: this.httpHeaders})
                    .pipe(
                      map( resp=>{
                        return resp
                      })
                    );
  }

  getInfoUser(request){    
    this.httpHeaders = new HttpHeaders({
      'Content-type': 'application/json', 
      'Authorization': 'Bearer '+ request.access_token,    
    }); 
    return this.http.get(this.apiUrl + "api/auth/user", {headers: this.httpHeaders})
                    .pipe(
                      map( resp=>{
                        return resp
                      })
                    );
  }


  logouth(request){
    this.httpHeaders = new HttpHeaders({
      'Content-type': 'application/json', 
      'Authorization': 'Bearer '+ request.access_token,    
    }); 
    return this.http.get(this.apiUrl + "api/auth/user", {headers: this.httpHeaders})
                    .pipe(
                      map( resp=>{
                        return resp
                      })
                    );
  }


}
