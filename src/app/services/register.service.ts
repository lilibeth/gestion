import { Injectable } from '@angular/core';
import { environment } from 'src/environments/environment';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { map } from 'rxjs/operators';

@Injectable({
  providedIn: 'root'
})
export class RegisterService {

  private apiUrl: string = environment.urlApi; 
  httpHeaders: HttpHeaders;


  constructor( private http: HttpClient) { 
    this.httpHeaders = new HttpHeaders({
      'Content-type': 'application/json',       
      'X-Requested-With': 'XMLHttpRequest',
      'Access-Control-Allow-Origin': "*"  ,
      'Accept': 'application/json'        
    }); 
  }


  register(request){    
    return this.http.post(this.apiUrl + "api/auth/signup", request, {headers: this.httpHeaders})
                    .pipe(
                      map( resp=>{
                        return resp['data']
                      })
                    );
  }
}
