import { Injectable } from '@angular/core';
import { environment } from 'src/environments/environment';
import { HttpHeaders, HttpClient } from '@angular/common/http';
import { map } from 'rxjs/operators';

@Injectable({
  providedIn: 'root'
})
export class BookService {

  private apiUrl: string = environment.urlApi; 
  httpHeaders: HttpHeaders;


  constructor( private http: HttpClient) { 
    this.httpHeaders = new HttpHeaders({
      'Content-type': 'application/json',         
    }); 
  }

  getBooks(request){
    return this.http.post("http://www.mocky.io/v2/5e46f571330000b17a0266b8", request, {headers: this.httpHeaders})
                    .pipe(
                      map( resp=>{                        
                        return resp['data']['books']
                      })
                    );
  }

  getBook(request){
    return this.http.post("http://www.mocky.io/v2/5e4758aa330000aa7a026798", request, {headers: this.httpHeaders})
                    .pipe(
                      map( resp=>{                        
                        return resp['data']['book']
                      })
                    );
  }

  createBook(request){
    return this.http.post("http://www.mocky.io/v2/5e477c543000005c0029494b", request, {headers: this.httpHeaders})
                    .pipe(
                      map( resp=>{                        
                        return resp['data']
                      })
                    );
  }


  updateBook(request){
    return this.http.post("http://www.mocky.io/v2/5e4788d63000007400294962", request, {headers: this.httpHeaders})
                    .pipe(
                      map( resp=>{                        
                        return resp['data']
                      })
                    );
  }
}
